package com.grega.test.cucumber.plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

@Mojo(name = "describe")
public class CLIMojo extends AbstractMojo {

	@Parameter(defaultValue = "${project}", required = true, readonly = true)
	private MavenProject project;

	private static String[] EXTENSIONS = { "feature", "template" };

	@Parameter(defaultValue = "${project.build.directory}")
	private String projectBuildDir;

	public void execute() throws MojoExecutionException, MojoFailureException {
		getLog().info("Scanning project: " + project.getName());
		getLog().info("Project build dir: " + projectBuildDir);
		// search for test resources
		List<Resource> testResources = project.getTestResources();
		for (Resource resource : testResources) {
			getLog().info("Scanning testResource: " + resource.getDirectory());
			String dir = resource.getDirectory();
			File directory = new File(dir);
			if (!directory.isDirectory()) {
				// skip resource
				continue;
			} else {
				Collection<File> listFiles = FileUtils.listFiles(directory, EXTENSIONS, Boolean.TRUE);
				StringBuilder sb = new StringBuilder();
				for (File file : listFiles) {
					String featureTag = describeFeatureFile(file);
					if (StringUtils.isNotBlank(featureTag)){
						getLog().info("Feature found: " +
								StringUtils.removeStart(file.getPath(), dir) + " TAG: "+
								featureTag);
						sb.append("TAG: ").append(featureTag).append( " --> ")
								.append(StringUtils.removeStart(file.getPath(), dir))
								.append(System.lineSeparator());
					}
					
				}
				writeDescriptionFile(sb);
			}

		}

	}

	private void writeDescriptionFile(StringBuilder sb) {

		try {
			// create the target dir if not exists
			File file = new File(projectBuildDir + File.separatorChar + "testList.dat");
			FileUtils.writeStringToFile(file, sb.toString(), Charset.defaultCharset());
						
		} catch (IOException e) {
			getLog().error(e);
		}
	}

	protected String describeFeatureFile(File file) {
		try (BufferedReader br = new BufferedReader(new FileReader(file))){
			String line = null;
			StringBuilder sb = new StringBuilder();

			while ((line = br.readLine()) != null) {
				if (StringUtils.startsWith(line, "@")) {
					sb.append(line);
					return sb.toString();
				} else if (StringUtils.startsWith(line.trim(), "Feature")) {
					sb.append("no tags found!");
					return sb.toString();
				}
			}
			return StringUtils.EMPTY;
		} catch (Exception e) {
			getLog().error(e);
			return StringUtils.EMPTY;
		}

	}

}
