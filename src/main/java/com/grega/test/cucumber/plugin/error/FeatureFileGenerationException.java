package com.grega.test.cucumber.plugin.error;

public class FeatureFileGenerationException extends Exception  {


	/**
	 * 
	 */
	private static final long serialVersionUID = -3523041852955602518L;

	public FeatureFileGenerationException(String message) {
		super(message);
	}

}
