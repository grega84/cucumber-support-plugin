package com.grega.test.cucumber.plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import com.grega.test.cucumber.plugin.error.FeatureFileGenerationException;
import com.grega.test.cucumber.plugin.error.XlsReadException;

import gherkin.AstBuilder;
import gherkin.Parser;
import gherkin.TokenMatcher;
import gherkin.ast.GherkinDocument;
@Mojo(
    name = "process")
public class ProcessorMojo extends AbstractMojo {
  
  private static final String ERROR_READING_FILE = "Error reading file :";
  private static final String ERROR_CREATING_FILE = "Error creating file :";
  private static final String ERROR_LOADING_DATASOURCE = "Error loading exteranal datasource :";
  private static final String ERROR_PLUGIN_EXECUTION = "Error plugin execution :";

@Parameter(
      defaultValue = "${project}", required = true, readonly = true)
  private MavenProject project;
  
  @Parameter(property = "datasource.path", required = false, readonly = true)
  private String dataSourcePath;
  
  @Parameter(property = "cucumber.options", required = false, readonly = true)
  private String cucumberOption;
  
  @Parameter(property = "scenario.prefix", required = false, readonly = true)
  private String scenarioPrefix;

  
  
  public void execute() throws MojoExecutionException, MojoFailureException {
	  
	  getLog().info("PROCESSING test resources, with cucumber option: " + cucumberOption);
        
    List<Resource> testResources = project.getTestResources();
    for (Resource resource : testResources) {
    	getLog().info("-->>> resources  " + resource.getDirectory());
      File resourceFile = new File(resource.getDirectory());
      if (resourceFile.exists()){
	      Iterator<File> files =
	          FileUtils.iterateFiles(resourceFile, new String[] { "template" }, Boolean.TRUE);
	      while (files.hasNext()) {
	        try {
	          File file = files.next();
	          boolean isCsvResource =checkCsvExtension(file);
	          if (!skipFile(file)){
	        	  scanFile(isCsvResource, file);
	          }
	        } catch (Exception e) {
	        	getLog().error(ERROR_PLUGIN_EXECUTION,e);
	          throw new MojoExecutionException(e.getMessage());
	        }
	      }
      }else{
    	  getLog().info("resource :" + resource.getDirectory() +" does note exist. Skipping this resource!");
      }
    }
    
  }

	private void scanFile(boolean isCsvResource, File file) throws FeatureFileGenerationException {
		if (isCsvResource){
			scanFile(file);
		  }else{
		  	xlsScanFile(file);
		  }
	}
  
  //skip the file if the tags are not applicable
  private boolean skipFile(File file){
	  getLog().info("Scanning file: " + file.getName());
	  String tagsInFile = FeatureUtils.describeFeatureFile(file);
	  getLog().info("Tags in this file: " + tagsInFile);
	  String tagsInOptions = StringUtils.substringAfter(cucumberOption, "--tags");
	  getLog().info("Tags to run: " + tagsInOptions);
	  if (StringUtils.isBlank(tagsInOptions)){
		  return Boolean.TRUE;
	  }
	  String[] tagsInFileAsList = StringUtils.splitPreserveAllTokens(tagsInFile);
	  String[] tagsInOptionAsList = StringUtils.splitPreserveAllTokens(tagsInOptions);
	  boolean disjoint = Collections.disjoint(Arrays.asList(tagsInFileAsList), Arrays.asList(tagsInOptionAsList));
	  if (disjoint){
		  getLog().info("Tags not applicable: skipping this file!");
	  }
	  // if the two collection have no element in common, skip the file
	  return disjoint;
  }
  
  private void xlsScanFile(File file) throws FeatureFileGenerationException {
    

  
    File localDirectory = file.getParentFile();
    String str;
	try {
		str = FileUtils.readFileToString(file,StandardCharsets.UTF_8);
	} catch (IOException e) {
	      getLog().error(ERROR_READING_FILE,e);
	      throw new FeatureFileGenerationException(ERROR_READING_FILE+ file.getName()+" " +e.getMessage());
	}
    TokenMatcher matcher = new TokenMatcher();
    Parser<GherkinDocument> parser = new Parser<>(new AstBuilder());
    GherkinDocument gherkinDocument = parser.parse(str, matcher); 
    Map<String, List<List<CellElement>>> datasourceMap;
	try {
		datasourceMap = ExternalDatasourceLoadUtils.loadExternalDatasoSourceData(gherkinDocument,localDirectory,dataSourcePath);
		GherkinDocument gherkindMergedDocument = MergeExternalDataUtils.mergeGherkinDocument(gherkinDocument,datasourceMap,scenarioPrefix);
		StringBuilder sb=GenerateFeatureFileUtils.generateFeatureFile(gherkindMergedDocument);
		String fileName = FilenameUtils.removeExtension(file.getName());
		File newFeatureFile = new File(localDirectory.getAbsolutePath() + File.separatorChar + fileName + ".feature");
		writeFeatureFile(sb, newFeatureFile);
	} catch (XlsReadException e) {
		getLog().error(ERROR_LOADING_DATASOURCE,e);
		throw new FeatureFileGenerationException(ERROR_LOADING_DATASOURCE+e.getMessage());
	}
	  if (StringUtils.isBlank(scenarioPrefix)){
		  throw new IllegalArgumentException("Missing scenarioPrefix parameter"); 
	  }    
  }

	private void writeFeatureFile(StringBuilder sb, File newFeatureFile) throws FeatureFileGenerationException {
		try (FileWriter fileWriter = new FileWriter(newFeatureFile, Boolean.FALSE)){
			fileWriter.write(sb.toString());
			fileWriter.flush();
		} catch (IOException e) {
			getLog().error(ERROR_CREATING_FILE,e);
			throw new FeatureFileGenerationException(ERROR_CREATING_FILE+ newFeatureFile.getName()+" " +e.getMessage());
		}
		getLog().info("Created merged file: "+ newFeatureFile.getName());
	}
  
  private void scanFile(File file) throws FeatureFileGenerationException {
	   File localDirectory = file.getParentFile();
	   StringBuilder sb = new StringBuilder();
	   
	   try(BufferedReader br = new BufferedReader(new FileReader(file))){
		    String line;
		    while ((line = br.readLine()) != null) {
		      sb.append(line);
		      sb.append("\n\r");
		      if (StringUtils.containsIgnoreCase(line, "Examples: @")) {
		        String dataFileName = StringUtils.substringAfter(line, "Examples: @");
		        File dataFile = loadDataFile(dataFileName, localDirectory);
		        while (!StringUtils.contains(line = br.readLine(), "|")) {
		          sb.append(line);
		          sb.append("\n\r");
		        }
		        sb.append(line);// add column names
		        sb.append("\n\r");
		        getLog().info("Merging data file: " + dataFile.getName());
		        FileAppender fileAppender = getFileAppender(dataFile);
		        fileAppender.appendData(sb, dataFile);
		      }
		    }
		    String fileName = FilenameUtils.removeExtension(file.getName());
		    File newFeatureFile = new File(localDirectory.getAbsolutePath() + File.separatorChar + fileName + ".feature");
		    try(FileWriter fileWriter = new FileWriter(newFeatureFile, Boolean.FALSE)){
			    fileWriter.write(sb.toString());
			    fileWriter.flush();
			    fileWriter.close();
		    } 
		    getLog().info("Created merged file: "+ newFeatureFile.getName());
	   }catch (IOException e) {
		      getLog().error("Cvs reader exception ",e);
	    	throw new FeatureFileGenerationException("Cvs reader exception :"+e.getMessage());
	   }
  }  
  
  
  private FileAppender getFileAppender(File dataFile) {
	    String extension = FilenameUtils.getExtension(dataFile.getName());
	    if (StringUtils.equalsIgnoreCase("csv", extension)){
	      return new CSVFileAppender();
	    }
	    throw new IllegalArgumentException("Data file extension not recognized: "+ extension);
	  }  
  
  private File loadDataFile(String dataFileName, File localDirectory){
	 if (dataSourcePath==null){
		getLog().info("Reading data file from local path: " + localDirectory);
		return new File(localDirectory.getAbsolutePath() + File.separatorChar + dataFileName);
	 }else{
		 //load from custom path
		 //check if dataFilePath is a deirectory
		 File dataFilePath =new File(dataSourcePath);	
		 if (!dataFilePath.isDirectory()){
			 throw new IllegalArgumentException("The property dataSourcePath must be a directory");
		 }
		 getLog().info("Reading data file from custom path: " + dataFilePath.getAbsolutePath());
		 return new File(dataFilePath.getAbsolutePath() + File.separatorChar + dataFileName);
	 }
	 
  }  
  
  private boolean checkCsvExtension(File dir) {
	  File localDirectory;
	  if (dataSourcePath==null){
		   localDirectory = dir.getParentFile();
	  }else{
		  // If the path is relative (not contemplated in old support for csv data loading, it return false)
		  if (StringUtils.isBlank(FilenameUtils.getPrefix(dataSourcePath))){
			  return false;
		  }
		  localDirectory =new File(dataSourcePath);	
		 if (!localDirectory.isDirectory()){
			 throw new IllegalArgumentException("The property dataSourcePath must be a directory");
		 }
		  
	  }
	  File[] csvFiles = localDirectory.listFiles((file,name)-> name.toLowerCase().endsWith(".csv"));
	  File[] xlsFiles = localDirectory.listFiles((file,name)->name.toLowerCase().endsWith(".xlsx"));
	  
	  if (csvFiles.length==0 && xlsFiles.length==0){
		  throw new IllegalArgumentException("datasource file not found");
	  }
	  
	  return csvFiles.length >0;
  }
}
