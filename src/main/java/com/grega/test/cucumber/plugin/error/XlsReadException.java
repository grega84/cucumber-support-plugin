package com.grega.test.cucumber.plugin.error;

public class XlsReadException extends Exception {


	/**
	 * 
	 */
	private static final long serialVersionUID = 3610504224487885780L;

	public XlsReadException(String message) {
		super(message);
	}

}
