package com.grega.test.cucumber.plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.grega.test.cucumber.plugin.error.FeatureFileGenerationException;

public class CSVFileAppender implements FileAppender{
  private static Logger logger = LoggerFactory.getLogger(CSVFileAppender.class);
	
  public StringBuilder appendData(StringBuilder sb, File dataFile) throws FeatureFileGenerationException{
    try(BufferedReader br = new BufferedReader(new FileReader(dataFile))){
	    String line = br.readLine(); // skip first row
	    while ((line = br.readLine()) != null) {
	      String[] splittedData = line.split(";");
	      StringBuilder lineToAdd = new StringBuilder();
	      lineToAdd.append("    |");
	      for (String token : splittedData) {
	        lineToAdd.append(token).append("|");
	      }
	      lineToAdd.append("\n\r");
	      sb.append(lineToAdd.toString());
	    } 
    } catch (IOException e) {
    	logger.error("Error Genenerating feature file: "+e.getMessage(),e);
    	throw new FeatureFileGenerationException("Error Genenerating feature file: "+e.getMessage());
	}
    return sb;
  }

}
