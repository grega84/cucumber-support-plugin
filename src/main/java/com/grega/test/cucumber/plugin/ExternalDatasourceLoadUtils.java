package com.grega.test.cucumber.plugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.grega.test.cucumber.plugin.error.XlsReadException;

import gherkin.ast.Comment;
import gherkin.ast.GherkinDocument;

/**
 * This class contains the method that retrieve the data from xls external data source file that will be merged 
 * with the cucumber table of the template feature file
 *
 */
public class ExternalDatasourceLoadUtils {
	
	private ExternalDatasourceLoadUtils(){
		 throw new IllegalAccessError("Rerource load utility class");
	}
	

	private static Logger logger = LoggerFactory.getLogger(ExternalDatasourceLoadUtils.class);
	
	/**
	 * This method retrieve the data from the external xls data source file 
	 * It populates a map with a composite key consisting of tableId and scenarioId of the xls file.
	 * The value corresponding to the composite key is the table that will be merged to a cucumber table inside the 
	 * template feature file   
	 * 
	 * 
	 * 
	 * @param gherkinDocument the GherkinDocument object with the populated cucumber table
	 * @param localDirectory the relative path of the resource folder that contains the xls external data source
	 * @param dataFilePath the absolute path of the folder that contains the xls external data source
	 * @return
	 * @throws XlsReadException 
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public static Map<String,List<List<CellElement>>> loadExternalDatasoSourceData(GherkinDocument gherkinDocument,File localDirectory,String dataFilePath ) throws XlsReadException  {
		List<Comment> commentList = gherkinDocument.getComments();
		Map<String,List<List<CellElement>>> scenarioMap = new HashMap<>();
		Boolean foundDatasource = false;
		for(Comment comment : commentList){
			if (comment.getText().contains("@datasource")){
				 String dataFileName = StringUtils.substringAfter(comment.getText(), "@datasource=");
				File dataFile = loadDataFile(dataFileName, localDirectory,dataFilePath);
				XSSFWorkbook workbook;
				try {
					workbook = new XSSFWorkbook(dataFile);
					FormulaEvaluator objFormulaEvaluator = new XSSFFormulaEvaluator(workbook);
					extractXlsData(scenarioMap, workbook, objFormulaEvaluator);
					workbook.close();
					foundDatasource = true;
				} catch (InvalidFormatException | IOException e) {
					if(logger.isErrorEnabled()){
						logger.error("External data load error : {}", localDirectory,e);
					}
					throw new XlsReadException(e.getMessage());
				}
			}
		}
		if (commentList.isEmpty()||!foundDatasource){
			throw new XlsReadException("Source file not found");
		}
		return scenarioMap;
	}

	
	/**
	 * 
	 * 
	 * @param dataFileName
	 * @param localDirectory the relative path of the resource folder that contains the xls external data source
	 * @param dataFilePath the absolute path of the folder that contains the xls external data source
	 * @return
	 * 
	 */
	private static File loadDataFile(String dataFileName, File localDirectory,String dataFilePath){
		 if (dataFilePath==null){
			 logger.info("Reading data file from local path: {}", localDirectory);
			 return new File(localDirectory.getAbsolutePath()  + File.separatorChar + dataFileName);	
		 }else if (StringUtils.isNotBlank(FilenameUtils.getPrefix(dataFilePath))){
			 File absoluteDirectory =new File(dataFilePath);
			 if (absoluteDirectory.exists()){
				 if (!absoluteDirectory.isDirectory()){
					 throw new IllegalArgumentException("The property datasourcePath must be a directory");
				 }
			 }else {
				 throw new IllegalArgumentException("The directory "+ dataFilePath + " does not exist");
			 }
			 return new File(dataFilePath+ File.separatorChar + dataFileName);	
		 } else {
			 String path =localDirectory+String.valueOf(File.separatorChar) + dataFilePath;
			 logger.info("Reading data file from local path: {}" , path);
			 	return new File(localDirectory.getAbsolutePath()+ File.separatorChar +dataFilePath  + File.separatorChar + dataFileName);
		 }
	}
	
	
	/**
	 * 
	 * Extract the data from xlsx data source file and 
	 * 
	 * 
	 * @param scenarioMap the map with scenarioId and tableId composite key and table as value
	 * @param workbook the apache poi object that with xls file mapping
	 * @param objFormulaEvaluator
	 * @throws IOException
	 * @throws InvalidFormatException
	 */
	private static void extractXlsData(Map<String, List<List<CellElement>>> scenarioMap, XSSFWorkbook workbook,
			FormulaEvaluator objFormulaEvaluator) throws XlsReadException {
		for (Sheet sheet : workbook) {
			int headerScenarioIdIndex = getIdHeaderIndex(sheet,true);
			int tableLength = getTableLength(headerScenarioIdIndex,sheet,objFormulaEvaluator);
			int headerTableIdIndex = getIdHeaderIndex(sheet,false);
			for (Row row : sheet) {
				if(row.getRowNum()>0 && row.getRowNum()<=tableLength){		
				    if (row.getCell(headerScenarioIdIndex)==null){
					  throw new XlsReadException("Error in getting scenarioId at column index  : " + headerScenarioIdIndex +" of  sheet " + sheet.getSheetName());
				    }
				    if (row.getCell(headerTableIdIndex)==null){
					  throw new XlsReadException("Error in getting tableId at column index  : " + headerTableIdIndex +" of sheet " + sheet.getSheetName());
				    }							  
					populateScenarioMap(scenarioMap, objFormulaEvaluator, sheet, headerScenarioIdIndex,headerTableIdIndex, row);
				}
			}
		}
	}

	
	
	/**
	 * 
	 * populate the map from xls file with table data to append to cucumber table of template file 
	 * 
	 * @param scenarioMap the map with scenarioId and tableId composite key and table as value
	 * @param objFormulaEvaluator
	 * @param sheet the apache poi object that xls sheet file mapping
	 * @param headerScenarioIdIndex the column index corresponding to scenarioId of the xls sheet file  
	 * @param headerTableIdIndex the column index corresponding to tableId of the xls sheet file 
	 * @param row
	 * @throws XlsReadException 
	 */
	private static void populateScenarioMap(Map<String, List<List<CellElement>>> scenarioMap,
			FormulaEvaluator objFormulaEvaluator, Sheet sheet, int headerScenarioIdIndex, int headerTableIdIndex,
			Row row) throws XlsReadException {
		List<CellElement> tableRow = new ArrayList<>();
		List<List<CellElement>> table =  scenarioMap.get(getStringCellType(row.getCell(headerScenarioIdIndex),objFormulaEvaluator).trim()+"_"+getStringCellType(row.getCell(headerTableIdIndex),objFormulaEvaluator).trim());
		if (table==null){
			table = new ArrayList<>();
		}
		for (int i=0;i<headerScenarioIdIndex;i++){
			if (row.getCell(i)==null){
				throw new XlsReadException("Error in getting cell at column index: "+ i + " inside row: "+row.getRowNum()+" of sheet: "+row.getSheet().getSheetName());
			}
			CellElement cell = new CellElement();
			if (logger.isDebugEnabled()){
				logger.debug("Processing cell: "+ getStringCellType(row.getCell(i),objFormulaEvaluator)+ " inside row: "+row.getRowNum()+" of sheet: "+row.getSheet().getSheetName());
			}
			cell.setCellHeader(getStringCellType(sheet.getRow(0).getCell(i),objFormulaEvaluator));
			cell.setCellValue(getStringCellType(row.getCell(i),objFormulaEvaluator));
			tableRow.add(cell);
		}		
		table.add(tableRow);
		scenarioMap.put(getStringCellType(row.getCell(headerScenarioIdIndex),objFormulaEvaluator).trim()+"_"+getStringCellType(row.getCell(headerTableIdIndex),objFormulaEvaluator).trim(), table);
	}
	
	/**
	 * Get the content of xls cell file in the specific data format and return it as string
	 * 
	 * @param cell he apache poi object that maps xls cell
	 * @param objFormulaEvaluator
	 * @return
	 */
	private static String getStringCellType(Cell cell, FormulaEvaluator objFormulaEvaluator){
		  DataFormatter objDefaultFormat = new DataFormatter();
		  objFormulaEvaluator.evaluate(cell);
		  return objDefaultFormat.formatCellValue(cell, objFormulaEvaluator);
		 }
	
	
	/**
	 * get the scenarioId column index if isScenarioId is true or tableId column index if isScenarioId is false
	 * 
	 * @param sheet the apache poi object that xls sheet file mapping
	 * @param isScenarioId variable to indicates if the index to return is the scenarioId or the tableId 
	 * @return
	 * @throws IOException
	 * @throws InvalidFormatException
	 */
	private static int getIdHeaderIndex(Sheet sheet,boolean isScenarioId) throws XlsReadException {
		int headerIdIndex=0;
			headerIdIndex = extractHeaderIdIndex(sheet, isScenarioId, headerIdIndex);
			if (headerIdIndex==0){
				if(isScenarioId){
					throw new XlsReadException("scenarioId not found for sheet : " + sheet.getSheetName() + " or scenarioId not found in the first row of sheet: " + sheet.getSheetName());
				} else {
					throw new XlsReadException("tableId not found for sheet : " + sheet.getSheetName() + " or tableId not found in the first row of sheet: " + sheet.getSheetName()); 	  
				}
			}
			else {
				return headerIdIndex;
			}
	}
	
	 
	/**
	 * Get the index of the last column of an xls sheet of a data source file
	 * As design rule the scenarioId must be the last column of xls sheet file 
	 * 
	 * @param scenarioIdIndex
	 * @param sheet
	 * @param objFormulaEvaluator
	 * @return
	 * @throws XlsReadException 
	 */
	private static int getTableLength(int scenarioIdIndex, Sheet sheet,FormulaEvaluator objFormulaEvaluator) throws XlsReadException {
		 int  tableSize = 0;
		  for (Row row : sheet) {
			  if (row.getCell(scenarioIdIndex)==null){
				  throw new XlsReadException("Error in getting scenarioId at column index  : " + scenarioIdIndex +" of sheet " + sheet.getSheetName());
			  }
			  String scenarioValue = getStringCellType(row.getCell(scenarioIdIndex),objFormulaEvaluator); 
			  if (StringUtils.isNotBlank(scenarioValue)){
				  tableSize = row.getRowNum();
			  }
		  }
			if (tableSize==0){
				throw new XlsReadException("No element found in : " + sheet.getSheetName());
			}
		return tableSize;
	}
	  
	private static int extractHeaderIdIndex(Sheet sheet, boolean isScenarioId, int headerIdIndex) throws XlsReadException {
		int idIndex = headerIdIndex;
		for (Row row : sheet) {
			for (Cell cell : row) {
				if (Cell.CELL_TYPE_STRING == cell.getCellType()) {
					idIndex = determineHeaderIdIndex(sheet, isScenarioId, headerIdIndex, row, cell);
					if (idIndex>0){
						return idIndex;
					}
				 }
			 }
		}
		return idIndex;
	}

	private static int determineHeaderIdIndex(Sheet sheet, boolean isScenarioId, int headerIdIndex, Row row,Cell cell) throws XlsReadException {
		int idIndex = headerIdIndex;
		if(isScenarioId){
			if (cell.getStringCellValue().trim().equalsIgnoreCase("scenarioId")){
				if(cell.getColumnIndex()!=(row.getLastCellNum()-2)) {
					throw new XlsReadException("scenarioId must be the second-last column inside sheet: " + sheet.getSheetName());
				}
				idIndex =cell.getColumnIndex();
			}
		 } else {
			if (cell.getStringCellValue().trim().equalsIgnoreCase("tableId")){
				if(cell.getColumnIndex()!=(row.getLastCellNum()-1)) {
					throw new XlsReadException("tableId must be the last column inside sheet: " + sheet.getSheetName());
				}
				idIndex= cell.getColumnIndex();
			}
		 }
		return idIndex;
	}
	
}
