package com.grega.test.cucumber.plugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;

@Mojo(
    name = "generate-report")
public class ReportingMojo extends AbstractMojo{
  
  @Parameter(
      defaultValue = "${project}", required = true, readonly = true)
  private MavenProject project;
  
   /**
   * Directory where saving the report files
   * @since 1.0.0
   */
  @Parameter(defaultValue="target/prettyReport" , property="cucumber.support.report.output.dir" )
  private File reportOutputDirectory;
  
  /**
   * Directory where reading the cucumber json files 
   * @since 1.0.0
   */
  @Parameter(defaultValue="target/reports", property="cucumber.support.report.source.dir")
  private File reportSourceDirectory;
  
  /**
   * Directory where reading images to embed in report
   * @since 1.0.0
   */
  @Parameter(defaultValue="target/embedded-images", property="report.images.dir")
  private File reportImageDirectory;

  public void execute() throws MojoExecutionException, MojoFailureException {
   getLog().info("Generating report for project " + project.getArtifactId());
    
   List<String> jsonFiles = new ArrayList<>();
   
   Iterator iterator = FileUtils.iterateFiles(reportSourceDirectory, new String[] {"json"}, Boolean.FALSE);
   
   while (iterator.hasNext()) {
    File file = (File) iterator.next();
    jsonFiles.add(file.getPath());
    
  }

   String buildNumber = project.getVersion();
   String projectName = project.getArtifactId();

   boolean runWithJenkins = false;
   boolean parallelTesting = false;

   Configuration configuration = new Configuration(reportOutputDirectory, projectName);
   
   // optionally only if you need
   configuration.setParallelTesting(parallelTesting);
   configuration.setRunWithJenkins(runWithJenkins);
   configuration.setBuildNumber(buildNumber);
      
   ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
   reportBuilder.generateReports(); 
   
   moveEmbeddedImages(configuration.getEmbeddingDirectory());
   
   getLog().info("Report generated in " + reportOutputDirectory.getAbsolutePath());
   
  }
  
  /**
   * move images from reportImageDirectory to embeddingDirectory
   * @param destDir
   */
  private void moveEmbeddedImages(File destDir){
	  try {
		//move only if the report image directory exists
		if (destDir !=null && destDir.exists()){
			FileUtils.copyDirectory(reportImageDirectory, destDir);
		}		
	} catch (IOException e) {
		getLog().error(e);
	}
	  
  }
  
}
