package com.grega.test.cucumber.plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gherkin.ast.Background;
import gherkin.ast.Comment;
import gherkin.ast.DataTable;
import gherkin.ast.Examples;
import gherkin.ast.Feature;
import gherkin.ast.GherkinDocument;
import gherkin.ast.Node;
import gherkin.ast.Scenario;
import gherkin.ast.ScenarioDefinition;
import gherkin.ast.ScenarioOutline;
import gherkin.ast.Step;
import gherkin.ast.TableCell;
import gherkin.ast.TableRow;
import gherkin.ast.Tag;


/**
 * This class contains the method that merge the data retrieved from xls external data source file with 
 * the cucumber table of the template feature file
 *
 */
public class MergeExternalDataUtils {

	private MergeExternalDataUtils(){
		 throw new IllegalAccessError("Rerource load utility class");
	}
	

	private static Logger logger = LoggerFactory.getLogger(MergeExternalDataUtils.class);
	
	
	public static GherkinDocument mergeGherkinDocument(GherkinDocument gherkinDocument,Map<String,List<List<CellElement>>> datasourceMap,String scenarioPrefix) {
		List<Comment> conmmentList = gherkinDocument.getComments();
		String scenarioId = "";
		List<ScenarioDefinition> scenarios = new ArrayList<>();
		  for (ScenarioDefinition scenarioDef : gherkinDocument.getFeature().getChildren()){
			  List<Examples> examples = new ArrayList<>();
			  scenarioId = determineScenarioId(scenarioDef,scenarioPrefix);
			  List<Step> stepList = new ArrayList<>();
			  buildStepDefinition(datasourceMap, conmmentList, scenarioId, scenarioDef, stepList);	
			  addAllScenarios(datasourceMap, conmmentList, scenarioId, scenarios, scenarioDef, examples, stepList);
		  }		
		Feature feature = new Feature(gherkinDocument.getFeature().getTags(), gherkinDocument.getFeature().getLocation(), gherkinDocument.getFeature().getLanguage(), gherkinDocument.getFeature().getKeyword(), gherkinDocument.getFeature().getName(), gherkinDocument.getFeature().getDescription(), scenarios);
		return new GherkinDocument(feature, gherkinDocument.getComments());
	}

	private static void addAllScenarios(Map<String, List<List<CellElement>>> datasourceMap, List<Comment> conmmentList,
			String scenarioId, List<ScenarioDefinition> scenarios, ScenarioDefinition scenarioDef,
			List<Examples> examples, List<Step> stepList) {
		if ( scenarioDef instanceof ScenarioOutline){
			  ScenarioOutline scenarioOutlineRes = buildScenarioOutline(datasourceMap, conmmentList, scenarioId,
					scenarioDef, examples, stepList);
			  scenarios.add(scenarioOutlineRes);
		  }else if ( scenarioDef instanceof Scenario) {
			  Scenario scenario = (Scenario) scenarioDef;
			  Scenario scenarioRes = new Scenario(scenario.getTags(),  scenario.getLocation(),scenario.getKeyword(),scenario.getName(),scenario.getDescription(), stepList);
			  scenarios.add(scenarioRes);
		  }else {
			  Background background = (Background) scenarioDef;
			  Background backgroundRes = new Background(background.getLocation(),background.getKeyword(),background.getName(),background.getDescription() ,stepList);
			  scenarios.add(backgroundRes);
		  }
	}

	private static ScenarioOutline buildScenarioOutline(Map<String, List<List<CellElement>>> datasourceMap,
			List<Comment> conmmentList, String scenarioId, ScenarioDefinition scenarioDef, List<Examples> examples,
			List<Step> stepList) {
		ScenarioOutline scenarioOutline =(ScenarioOutline) scenarioDef;
		  Examples exampleRes =null;
		  for (Examples example : scenarioOutline.getExamples()){
			  List<TableRow> tb = mergeExampleTable(datasourceMap, conmmentList, scenarioId, example);
			  exampleRes = new Examples(example.getLocation(), example.getTags(), example.getKeyword(), example.getName(), example.getDescription(), example.getTableHeader(), tb);
			  examples.add(exampleRes);
		  }
		  ScenarioOutline scenarioOutlineRes =new ScenarioOutline(scenarioOutline.getTags(), scenarioOutline.getLocation(), scenarioOutline.getKeyword(),scenarioOutline.getName(), scenarioOutline.getDescription(), stepList, examples);
		return scenarioOutlineRes;
	}

	private static List<TableRow> mergeExampleTable(Map<String, List<List<CellElement>>> datasourceMap,
			List<Comment> conmmentList, String scenarioId, Examples example) {
		int tableIdIntex = example.getLocation().getLine()+1;
		  List<List<CellElement>> tableValuesExtracted = extractTableValues(datasourceMap, conmmentList, scenarioId,example.getName(), tableIdIntex,false);
		  TableRow headerRow = example.getTableHeader();
		  List<List<CellElement>> tableValues= extractTableWithMatchingColumn(headerRow,tableValuesExtracted);
		  List<TableRow> tb = new ArrayList<>();
		  for (List<CellElement> datasourceTableRow: tableValues){
			 List<TableCell> cells = new ArrayList<>();
			 for ( CellElement datasourceCell:datasourceTableRow){
				TableCell cell = new TableCell(null, datasourceCell.getCellValue());
				cells.add(cell);
			 }
			 TableRow tableRow = new TableRow(null, cells);
			 tb.add(tableRow);
		  }
		return tb;
	}

	private static void buildStepDefinition(Map<String, List<List<CellElement>>> datasourceMap,
			List<Comment> conmmentList, String scenarioId, ScenarioDefinition scenarioDef, List<Step> stepList) {
		for (Step step : scenarioDef.getSteps()){
			  Node argument =step.getArgument();
			  if ((step.getArgument() !=null )&& (step.getArgument() instanceof DataTable)){
				  int tableIdIntex = step.getArgument().getLocation().getLine()-1;
				  List<List<CellElement>> tableValuesExtracted = extractTableValues(datasourceMap, conmmentList, scenarioId,step.getText(), tableIdIntex,true);
				  DataTable dataTable = (DataTable)step.getArgument();
				  List<TableRow> rows = mergeDataTable(tableValuesExtracted, dataTable);
				 argument = new DataTable(rows);
			  }
			Step stepDatasource = new Step(step.getLocation(),step.getKeyword(),step.getText(), argument);
			stepList.add(stepDatasource);
		  }
	}

	private static List<TableRow> mergeDataTable(List<List<CellElement>> tableValuesExtracted, DataTable dataTable) {
		List<TableRow> rows = new ArrayList<>();
		  rows.addAll(dataTable.getRows());
		  TableRow headerRow = dataTable.getRows().get(0);
		  if((tableValuesExtracted!=null) && (!tableValuesExtracted.isEmpty())){
			  List<List<CellElement>> tableValues= extractTableWithMatchingColumn(headerRow,tableValuesExtracted);
			  for (List<CellElement> datasourceTableRow: tableValues){
				 List<TableCell> cells = new ArrayList<>();
				 for ( CellElement datasourceCell:datasourceTableRow){
					TableCell cell = new TableCell(null, datasourceCell.getCellValue());
					cells.add(cell);
				 }
				 TableRow appendedTableRow = new TableRow(null, cells);
				 rows.add(appendedTableRow);
			  }		  
		 }
		return rows;
	}

	private static List<List<CellElement>> extractTableWithMatchingColumn(TableRow headerRow, List<List<CellElement>> tableValuesExtracted) {
		List<List<CellElement>> tableElement= new ArrayList<>();
		List<String> excelHeaders = new ArrayList<>();
		for (List<CellElement> cellList :tableValuesExtracted){
			List<CellElement> cellListResult = new ArrayList<>(); 
			for (CellElement cellElement:cellList){
				for (TableCell cell:headerRow.getCells()){
					if (cellElement.getCellHeader().trim().equalsIgnoreCase(cell.getValue().trim())){
						cellListResult.add(cellElement);
						
						if (excelHeaders.size()!=cellList.size()){
							excelHeaders.add(cellElement.getCellHeader().trim());
						}
					}
				}
			}
			tableElement.add(cellListResult);
		}
		checkMatchingHeader(headerRow, excelHeaders);
		return tableElement;
	}

	private static void checkMatchingHeader(TableRow headerRow, List<String> excelHeaders) {
		if(excelHeaders.size()!=headerRow.getCells().size()){
			Boolean foundElement = false;
			for (TableCell cell:headerRow.getCells()){
				for (String header:excelHeaders){
					if (cell.getValue().equals(header)){
						foundElement=true;
						break;
					}else{
						foundElement = false;
					}
					
				}
				if (!foundElement){
					logger.warn("No matching between " + cell.getValue()+ " header column on line "+ headerRow.getLocation().getLine()+" and excel header column") ;
				}
			}
		}
	}

	private static List<List<CellElement>> extractTableValues(
			Map<String,List<List<CellElement>>> datasourceMap, List<Comment> conmmentList,
			String scenarioId, String exceptionObjectName, int tableIdIntex,Boolean isDatatable) {
		String tableKey = "";
		for (Comment comment:conmmentList){
			if(comment.getLocation().getLine()==tableIdIntex){
				tableKey=StringUtils.substringAfter(comment.getText(), "#@").trim();
				break;
			}
		}
		  List<List<CellElement>> tableValues = datasourceMap.get(scenarioId+"_"+tableKey);
		  if(tableValues==null){
			  if (isDatatable){
				  if(logger.isWarnEnabled()){
					  logger.warn("No datatable found for step : " + exceptionObjectName + " line " + tableIdIntex +" of template file");
				  }
			  }else{
				  if(logger.isWarnEnabled()){
					  logger.warn("No example found for : " + exceptionObjectName + " line " + tableIdIntex +" of template file");
				  }
			  }
		  }
		return tableValues;
	}
	
	private static String determineScenarioId(ScenarioDefinition scenarioDef,String scenarioPrefixParam) {
		String scenarioId="";
		Boolean foundPrefix=false;
		if (StringUtils.isBlank(scenarioPrefixParam)){
			throw new IllegalArgumentException("The property scenario.prefix is blank! Please set the property scenario.prefix with the proper prefix for scenarios used in template file.");
		}
		String scenarioPrefix = scenarioPrefixParam+"=";
		if ( scenarioDef instanceof ScenarioOutline){
			  scenarioId = extractScenarioIdFromScenarioOutline(scenarioDef, scenarioPrefixParam, scenarioId, foundPrefix,
					scenarioPrefix);
		  }else if ( scenarioDef instanceof Scenario){
			  scenarioId = extractScenarioIdFromScenario(scenarioDef, scenarioPrefixParam, scenarioId, foundPrefix,
					scenarioPrefix);			  
		  }else if ( scenarioDef instanceof Background){
			  scenarioId="Background";
		  }
		return scenarioId;
	}

	private static String extractScenarioIdFromScenario(ScenarioDefinition scenarioDef, String scenarioPrefixParam,
			String scenarioId, Boolean foundPrefix, String scenarioPrefix) {
		Boolean checkPrefix = foundPrefix;
		String id = scenarioId;
		Scenario scenario =(Scenario) scenarioDef;
		  for (Tag tag :scenario.getTags()){
			  if (tag.getName().contains(scenarioPrefix)){
				  checkPrefix=true;
				  id =  StringUtils.substringAfter(tag.getName(), scenarioPrefix);
			  } 
		  }	
		  if (!checkPrefix) {
			  throw new IllegalArgumentException("The prefix "+scenarioPrefixParam+" set for scenario.prefix property doesn't match the prefix in the tag of scenario at line "+scenarioDef.getLocation().getLine()+" of template file.");		  
		  }
		return id;
	}

	private static String extractScenarioIdFromScenarioOutline(ScenarioDefinition scenarioDef,
			String scenarioPrefixParam, String scenarioId, Boolean foundPrefix, String scenarioPrefix) {
		ScenarioOutline scenarioOutline =(ScenarioOutline) scenarioDef;
		Boolean checkPrefix = foundPrefix;
		String id = scenarioId;		
		  for (Tag tag :scenarioOutline.getTags()){
			  if (tag.getName().contains((scenarioPrefix))){
				  checkPrefix=true;
				  id =  StringUtils.substringAfter(tag.getName(), scenarioPrefix);
			  } 
		  } 
		  if (!checkPrefix){
			  throw new IllegalArgumentException("The prefix "+scenarioPrefixParam+" set for scenario.prefix property doesn't match the prefix in the tag of scenario at line "+scenarioDef.getLocation().getLine()+" of template file.");		  
		  }
		return id;
	}
	
}
