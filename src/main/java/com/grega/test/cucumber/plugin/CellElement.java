package com.grega.test.cucumber.plugin;

public class CellElement {
	
	private String cellHeader;
	private String cellValue;
	public String getCellHeader() {
		return cellHeader;
	}
	public void setCellHeader(String cellHeader) {
		this.cellHeader = cellHeader;
	}
	public String getCellValue() {
		return cellValue;
	}
	public void setCellValue(String cellValue) {
		this.cellValue = cellValue;
	}
	
	
	
}
