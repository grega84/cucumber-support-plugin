package com.grega.test.cucumber.plugin;

import java.io.File;

import com.grega.test.cucumber.plugin.error.FeatureFileGenerationException;

public interface FileAppender {
	StringBuilder appendData(StringBuilder sb, File dataFile) throws FeatureFileGenerationException;
}
