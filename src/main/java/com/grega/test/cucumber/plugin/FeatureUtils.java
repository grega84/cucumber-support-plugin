package com.grega.test.cucumber.plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeatureUtils {
	
	private static Logger logger = LoggerFactory.getLogger(FeatureUtils.class);
	
	  private FeatureUtils() {
	  
	  }
	
	public static String describeFeatureFile(File file) {
		try (BufferedReader br = new BufferedReader(new FileReader(file))){
			String line = null;
			StringBuilder sb = new StringBuilder();

			while ((line = br.readLine()) != null) {
				if (StringUtils.startsWith(line, "@")) {
					sb.append(line);
					return sb.toString();
				} else if (StringUtils.startsWith(line.trim(), "Feature")) {
					
					return StringUtils.EMPTY;
				}
			}
			return StringUtils.EMPTY;
		} catch (Exception e) {
			if (logger.isErrorEnabled()){
			logger.error("Error "+e.getMessage(),e);
			}
			return StringUtils.EMPTY;
		}

	}

}
