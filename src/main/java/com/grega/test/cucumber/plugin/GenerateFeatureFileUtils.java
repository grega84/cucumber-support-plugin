package com.grega.test.cucumber.plugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gherkin.ast.Comment;
import gherkin.ast.DataTable;
import gherkin.ast.Examples;
import gherkin.ast.GherkinDocument;
import gherkin.ast.Scenario;
import gherkin.ast.ScenarioDefinition;
import gherkin.ast.ScenarioOutline;
import gherkin.ast.Step;
import gherkin.ast.TableCell;
import gherkin.ast.TableRow;
import gherkin.ast.Tag;

/**
 * This class contains the method for generate the feature file
 *
 */
public class GenerateFeatureFileUtils {
	
	
	private static Logger logger = LoggerFactory.getLogger(ExternalDatasourceLoadUtils.class);
	
	
	private GenerateFeatureFileUtils(){
		 throw new IllegalAccessError("Utility class");
	}
	
	/**
	 * 
	 * Generate the feature file content starting from GherkinDocument object 
	 * 
	 * @param gherkinDocument the GherkinDocument object with the populated cucumber table
	 * @return the content of feature file
	 */
	public static StringBuilder generateFeatureFile(GherkinDocument gherkinDocument){
		  StringBuilder str  = new StringBuilder();
		  List<Comment> commentList = gherkinDocument.getComments();
		  String content ="";
		  commentAppender(commentList,str,content,0);
		  for (  Tag tag : gherkinDocument.getFeature().getTags()){
			  content =tag.getName() + " ";
			  commentAppender(commentList,str,content,tag.getLocation().getLine());
		  }
		  str.append("\n");
		  content ="Feature: "+gherkinDocument.getFeature().getName()+"\n";
		  commentAppender(commentList,str,content,gherkinDocument.getFeature().getLocation().getLine());
		  if (StringUtils.isNotBlank(gherkinDocument.getFeature().getDescription())){
			  content =gherkinDocument.getFeature().getDescription()+"\n";
			  str.append(content);
		  }	  
		  str.append("\n");	  
		  generateScenarioDefinition(gherkinDocument, str, commentList);
		  if(logger.isInfoEnabled()){
			  logger.info(str.toString());
		  }
		  return str;
	  }

	/**
	 * 
	 * Generate the scenarios of feature file
	 * 
	 * @param gherkinDocument the GherkinDocument object with the populated cucumber table
	 * @param str the partial content of feature file
	 * @param commentList the List of comment if initial feature file contained in the GherkinDocument object
	 */
	private static void generateScenarioDefinition(GherkinDocument gherkinDocument, StringBuilder str,
			List<Comment> commentList) {
		String content;
		for (ScenarioDefinition scenarioDef : gherkinDocument.getFeature().getChildren()){
			  generateScenarioTags(str, commentList, scenarioDef);
			  commentBackwardAppender(commentList, str, scenarioDef.getLocation().getLine());
			  content =scenarioDef.getKeyword() +": "+ scenarioDef.getName()+"\n";
			  commentAppender(commentList,str,content,scenarioDef.getLocation().getLine());
			 
			  if (StringUtils.isNotBlank(scenarioDef.getDescription())){
				  content =scenarioDef.getDescription() +"\n";
				  str.append(content);
			  }				  
			  generateStepDefinition(str, commentList, scenarioDef);
			  str.append("\n");
			  if ( scenarioDef instanceof ScenarioOutline){
				  ScenarioOutline scenarioOutline =(ScenarioOutline) scenarioDef;
				  generateExamplesDefinition(str, commentList, scenarioOutline);
				  str.append("\n");
			  }
			  str.append("\n");
		  }
	}

	/**
	 * 
	 * Generate the scenario outline Examples
	 * 
	 * @param str the partial content of feature file
	 * @param commentList the List of comment if initial feature file contained in the GherkinDocument object
	 * @param scenarioOutline the scenario outline with the examples populated from the external resource file
	 * 
	 */
	private static void generateExamplesDefinition(StringBuilder str, List<Comment> commentList,
			ScenarioOutline scenarioOutline) {
		String content;
		for (Examples example : scenarioOutline.getExamples()){
			  commentBackwardAppender(commentList, str, example.getLocation().getLine());
			  content ="Examples: "+example.getName()+"\n";
			  commentAppender(commentList,str,content,example.getLocation().getLine());					  
			  if (StringUtils.isNotBlank(example.getDescription())){
				  content =example.getDescription()+"\n";
				  commentAppender(commentList,str,content,example.getLocation().getLine()+1);							  
			  }
			  for (TableCell cellHeader : example.getTableHeader().getCells()){
				  str.append("|"+cellHeader.getValue());
			  }
			  str.append("|\n");
			  for (TableRow tableRow : example.getTableBody()){
				 for (TableCell cell : tableRow.getCells()){
					 str.append("|"+cell.getValue());
				 }
				 str.append("|\n");				  
			  }
		  }
	}

	
	/**
	 * 
	 * Generate the steps with the data tables if they exists
	 * 
	 * @param str the partial content of feature file
	 * @param commentList the List of comment if initial feature file contained in the GherkinDocument object
	 * @param scenarioDef the step with the data tables if they exists
	 * 
	 */
	private static void generateStepDefinition(StringBuilder str, List<Comment> commentList,
			ScenarioDefinition scenarioDef) {
		String content;
		for (Step step : scenarioDef.getSteps()){
			  content =step.getKeyword() +" "+ step.getText()+"\n";
			  commentAppender(commentList,str,content,step.getLocation().getLine());			  
			  if ((step.getArgument() !=null )&& (step.getArgument() instanceof DataTable)){
				  DataTable dataTable = (DataTable)step.getArgument();
				  for (TableRow tableRow : dataTable.getRows()){
					 for (TableCell cell : tableRow.getCells()){
						 str.append("|"+cell.getValue());
					 }
					 str.append("|\n");				  
				  }
			  }
		  }
	}

	/**
	 * 
	 * Generate the scenario tags
	 * 
	 * @param str the partial content of feature file
	 * @param commentList the List of comment of initial feature file contained in the GherkinDocument object
	 * @param scenarioDef the scenario with the tags which you want to create  
	 */
	private static void generateScenarioTags(StringBuilder str, List<Comment> commentList,ScenarioDefinition scenarioDef) {
		String content;
		int tagPosition;
		if ( scenarioDef instanceof ScenarioOutline){
			  ScenarioOutline scenarioOutline =(ScenarioOutline) scenarioDef;
			  for (  Tag tag : scenarioOutline.getTags()){
				  Tag firstTag = scenarioOutline.getTags().get(0);
				  // This condition happened in case of multiple comments above  in-line tags
				  // It is in fact necessary to check if after the first tag there is a tag which is on the same line
				  if(firstTag.getLocation().getLine()!=tag.getLocation().getLine()||scenarioOutline.getTags().indexOf(tag)==0){
					  commentBackwardAppender(commentList, str, tag.getLocation().getLine());
				  }
				  content =tag.getName() + " ";
				  tagPosition = tag.getLocation().getLine();
				  commentAppender(commentList,str,content,tagPosition);				  
			  }
			  str.append("\n");	
		  } else  if ( scenarioDef instanceof Scenario){
			  Scenario scenario =(Scenario) scenarioDef;
			  for (  Tag tag : scenario.getTags()){
				  commentBackwardAppender(commentList, str, tag.getLocation().getLine());
				  content =tag.getName() + " ";
				  tagPosition = tag.getLocation().getLine();
				  commentAppender(commentList,str,content,tagPosition);				  
			  }
			  str.append("\n");	
		  }
	}
	  
	 /**
	  * It generates the feature file element and starting from the next line of the line position 
	  * of this specific element it appends a comment (if it exists) 
	  * 
 	  * @param commentList the List of comment of initial feature file contained in the GherkinDocument object
	  * @param sb the partial content of feature file
	  * @param content the element which we want to create
	  * @param linePosition the line position of the element which we want to create
	  * 
	  */
	private static void commentAppender(List<Comment>commentList,StringBuilder sb,String content,int linePosition){
		 int commentLine =0;
		 boolean isAppended=false;
		 for (Comment comment: commentList){
			 commentLine =comment.getLocation().getLine();
			 if (commentLine==linePosition +1){
				 sb.append(content);
				 sb.append(comment.getText()+"\n");
				 int commentIndex = commentList.indexOf(comment);
				 if (commentIndex<commentList.size()){
					 isAppended = append(commentList, sb, isAppended, commentIndex);
						if (isAppended){
							return;
						}					 
				 }
				 return;
			 }else if (comment.getLocation().getLine()==linePosition){
				 sb.append(content + " " +comment.getText()+"\n");
				 return;
			 }
		 }
		 sb.append(content);
	 }

	private static boolean append(List<Comment> commentList, StringBuilder sb, boolean isAppended, int commentIndex) {
		int currentCommentLine =0;
		 int nextCommentLine = 0;				 
		 for(int i = commentIndex;i<commentList.size()-1;i++){
			 currentCommentLine = commentList.get(i).getLocation().getLine();
			 nextCommentLine = commentList.get(i+1).getLocation().getLine();
			// Check after each iteration if there is a comment immediately after the actual comment and 
			// append it to the string buffer. Note that when there are no comment immediately after the actual 
			// comment the else condition return the string buffer with all comment on the element						 
			 if (nextCommentLine == currentCommentLine+1){
				 sb.append(commentList.get(i+1).getText()+"\n");
			 }else{
				 return true;
			 }
		 }
		return isAppended;
	}
	 
	
	 /**
	  * It appends a comment (if it exists) before a specific feature file element in the specific line position
	  * 
	  * It is used in some specific case like for example comment before tags or cucumber examples table
	  * 
	  * @param commentList the List of comment of initial feature file contained in the GherkinDocument object
	  * @param sb the partial content of feature file
	  * @param content the element which we want to create
	  * @param linePosition the line position of the element which we want to create
	  * 
	  */	
	 private static void commentBackwardAppender(List<Comment> commentList, StringBuilder sb, int linePosition) {
		int commentLine = 0;
		List<String>bufferList = new ArrayList<>(); 
		boolean isReverseAppend=false;
		for (Comment comment : commentList) {
			commentLine = comment.getLocation().getLine();
			if (commentLine == linePosition - 1) {
				bufferList.add(comment.getText() + "\n");
				int commentIndex = commentList.indexOf(comment);
				if (commentIndex < commentList.size()) {
					isReverseAppend = backwardAppend(commentList, sb, bufferList, isReverseAppend, commentIndex);
					if (isReverseAppend){
						return;
					}
				}
				sb.append(comment.getText() + "\n");
				return;
			}
		}
	}

	private static boolean backwardAppend(List<Comment> commentList, StringBuilder sb, List<String> bufferList,
			boolean isReverseAppend, int commentIndex) {
		int currentCommentLine = 0;
		int nextCommentLine = 0;
		for (int i = commentIndex; i >= 1; i--) {
			currentCommentLine = commentList.get(i).getLocation().getLine();
			nextCommentLine = commentList.get(i - 1).getLocation().getLine();
			// Check after each iteration if there is a comment immediately before the actual comment and 
			// add it to a bufferList. Note that when there are no comment immediately before the actual 
			// comment the else condition call the reverseAppend method that print all comment on the element
			// in reverse order (because the bufferList was populated with the following order: the last 
			// comment on the tag is the first comment in the bufferList
			if (nextCommentLine == currentCommentLine - 1) {
				bufferList.add(commentList.get(i - 1).getText() + "\n");
				
			} else {
				reverseAppend(sb, bufferList);
				return true;
			}
		}
		return isReverseAppend;
	}

	private static void reverseAppend(StringBuilder sb, List<String> bufferList) {
		Collections.reverse(bufferList);
		for (String buffer :bufferList){
			sb.append(buffer);
		}
	}
	
}
