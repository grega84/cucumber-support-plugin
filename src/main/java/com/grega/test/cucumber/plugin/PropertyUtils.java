package com.grega.test.cucumber.plugin;

import org.apache.commons.lang.StringUtils;

public class PropertyUtils {

	private PropertyUtils(){
		
	}
	
  public static String getReportFolder(String property) {
    
    String substringBetween = StringUtils.substringBetween(property, "json:", "json");
    if (StringUtils.isNotBlank(substringBetween)){
      return substringBetween + "json";
    }
    return StringUtils.EMPTY;
  
  }

}
