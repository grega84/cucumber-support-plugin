package com.grega.test.cucumber.plugin;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.grega.test.cucumber.plugin.error.FeatureFileGenerationException;

public class XLSFileAppender implements FileAppender {

	private static Logger logger = LoggerFactory.getLogger(XLSFileAppender.class);
	
	
	
  public StringBuilder appendData(StringBuilder sb, File dataFile) throws FeatureFileGenerationException {
	   try( XSSFWorkbook workbook = new XSSFWorkbook(dataFile)){
		    XSSFSheet sheet = workbook.getSheet("testData");
		   
		    if (sheet==null){
		      throw new InvalidFormatException("No sheet testData found!");
		    }
		    Iterator<Row> rowIterator = sheet.iterator();
		    if (rowIterator.hasNext()){
		      //skip column name
		      rowIterator.next();
		    }
		    
		    while (rowIterator.hasNext()) {
		      Row columnHeader = rowIterator.next();
		      // For each row, iterate through each columns
		      Iterator<Cell> cellIterator = columnHeader.cellIterator();
		      StringBuilder row = new StringBuilder();
		      Boolean checkNullContent = Boolean.TRUE;
		      while (cellIterator.hasNext()){
		        Cell cell = cellIterator.next();
		        String value = null;
		        if ( Cell.CELL_TYPE_NUMERIC == cell.getCellType()){
		          value = String.valueOf(cell.getNumericCellValue()).trim();
		        }else{
		         value = cell.getStringCellValue().trim();
		        }    
		        if (StringUtils.isNoneBlank(value)){
		          checkNullContent = Boolean.FALSE;
		        }
		        row.append("|");
		        row.append(value);
		      }
		      if (!checkNullContent){
		        sb.append(row.toString()).append("|").append("\n\r");
		      }
		     
		      
		    }  
		    
	   } catch (InvalidFormatException | IOException e) {
		   logger.error("Error writing feature file "+e.getMessage(),e);
		   throw new FeatureFileGenerationException(e.getMessage());
	}
	   return sb;
  }
  
}
