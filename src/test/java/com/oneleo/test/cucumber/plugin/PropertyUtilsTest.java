package com.oneleo.test.cucumber.plugin;

import org.junit.Assert;
import org.junit.Test;

import com.grega.test.cucumber.plugin.PropertyUtils;

public class PropertyUtilsTest {
  
  @Test
  public void testExtractReportPath(){
    String property = "--plugin json:target/report.json"; 
    String expectedFolder = "target/report.json";
    
    Assert.assertEquals(expectedFolder, PropertyUtils.getReportFolder(property));
    
    
  }
  
  @Test
  public void testExtractReportPath2(){
    String property = " --features = \"src/test/java/features/\" --plugin json:target/report.json --tags = \"@Signup-DataDriven\""; 
    String expectedFolder = "target/report.json";
    
    Assert.assertEquals(expectedFolder, PropertyUtils.getReportFolder(property));
    
    
  }

}
