package com.oneleo.test.cucumber.plugin;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.maven.execution.DefaultMavenExecutionRequest;
import org.apache.maven.execution.MavenExecutionRequest;
import org.apache.maven.plugin.testing.MojoRule;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuilder;
import org.apache.maven.project.ProjectBuildingException;
import org.apache.maven.project.ProjectBuildingRequest;
import org.codehaus.plexus.component.repository.exception.ComponentLookupException;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.grega.test.cucumber.plugin.ProcessorMojo;

import gherkin.AstBuilder;
import gherkin.Parser;
import gherkin.TokenMatcher;
import gherkin.ast.DataTable;
import gherkin.ast.Examples;
import gherkin.ast.GherkinDocument;
import gherkin.ast.ScenarioDefinition;
import gherkin.ast.ScenarioOutline;
import gherkin.ast.Step;
import gherkin.ast.TableCell;
import gherkin.ast.TableRow;
public class ComplexExternalXlsAutomaticMappingSupportTest {
	private Logger logger = LoggerFactory.getLogger(ComplexExternalXlsAutomaticMappingSupportTest.class);

	@Rule
	public MojoRule rule = new MojoRule();
	
	private MavenProject project;
	private static final String GENERATE_GOAL = "process";
	private String projectDir = System.getProperty(ConstantUtil.USER_DIR_PROPERTY);
	private String baseDirPath; 
	
	@Before
	public final void prepareEnvironment() {
		

		logger.info("***** SETUP Environment *****");	
		
		
		this.baseDirPath = projectDir + ConstantUtil.PROJECT_TEST_PATH+ ConstantUtil.COMPLEX_EXTERNAL_XLSX_AUTOMATIC_SUPPORT;
		File basedir = new File(baseDirPath);
		this.project = createMavenProject(basedir);
		
	}
	

	@Test
	public void testSuccessGeneration() throws Exception {
		
		logger.info("***** STARTING TEST WITH CORRECT GENERATION");	
		ProcessorMojo processorMojo = (ProcessorMojo) this.rule.lookupConfiguredMojo(this.project, GENERATE_GOAL);		
		Assert.assertNotNull(processorMojo);
		processorMojo.execute();
		
		File basedir = new File(baseDirPath);
		File featureFile = new File(basedir, "complexExternalAutomaticMapping.feature"); 
		Assert.assertTrue(featureFile.exists());
		
		logger.info("***** SUCCESS GENERATION");
	    String str = FileUtils.readFileToString(featureFile,StandardCharsets.UTF_8);
	    TokenMatcher matcher = new TokenMatcher();
	    Parser<GherkinDocument> parser = new Parser<>(new AstBuilder());	    
		List<String[][]> tables = initializeData();
	    
	    evaluateTables(str, matcher, parser, tables);	
	}
	
	private List<String[][]> initializeData() {
		
		String [][] table1  = { {"Daryl","Dixon","02/27/1977","Male","8161234567","reference"}}; 
		String [][] table2  = { {"Robert","Anderson","02/27/1999","Male","3128782504","reference"}}; 
	    String [][] table3  = {{"<firstName>","<lastName>","<dateOfBirth>","<phoneNumber>"}}; 	   
		String [][] table4  = { {"","Anderson","",""},{"","","","3128782504"},{"","","02/27/1999",""}}; 
	    String [][] table5  = { {"Ryan","Dixon"," 02/27/1988","Male","3128780204","reference"},{"Michael","Smith","08/22/1990","Male","3128780144","reference"},{"Michael","Smith","03/16/2004","Male","3128789024","reference"}}; 
	    String [][] table6  = {{"<firstName>","<lastName>","<dateOfBirth>","<phoneNumber>"}}; 
	    String [][] table7  = { {"Ryan","Dixon","02/27/1988 ","3128780204"},{"Ryan","","","3128780204"},{"R","","","3128780204"},{"","","02/27/1988 ","3128780204"},{"","Dixon","","3128780204"},{"Michael","Smith","",""},{"M","Smith","",""},{"M","","08/22/1990",""},{"M","","","3128789024"}}; 	    
	    String [][] table8  = { {"Nick","Jobs"," 02/27/1906","Male","3128781304","reference"}}; 	 
	    String [][] table9  = { {"<firstName>","<lastName>","<dateOfBirth>","<phoneNumber>"}}; 	 
	    String [][] table10  = { {"N","","",""},{"Nick","","",""}}; 	 
	    String [][] table11  = { {"John-carter","Smith","02/27/2010","Male","","specific"},{"John","Smith"," 02/27/1999","Male","","specific"},{"John","Smith"," 02/27/1999","Male","8171237369","specific"},{"Joel","Smith"," 02/27/1999","Male","","specific"},{"J","Smith"," 02/27/1999","Male","","specific"},{" John Carter","Smith"," 02/27/1999","Male","","specific"}}; 	 
	    String [][] table12  = { {"<firstName>","<lastName>","<dateOfBirth>","<phoneNumber>"}};
	    String [][] table13  = { {"J","Smith","",""},{"John","Smith","",""}}; 	 
	    String [][] table14  = { {"Matthew","Lardus","Male","specific"}}; 	
	    String [][] table15  = { {"Frank","Torrent","08/15/1958 ","Male","3652189730"}}; 	
	    String [][] table16  = { {"Frank","Torrent","08/15/1958 ","3652189730"}}; 	
	    String [][] table17  = { {" John-carter","Smith","02/27/2010","Male","specific"},{"D'Juan","Smith","02/27/1999","Male","specific"},{"Daril","O-Connor","02/27/1999","Male","specific"},{"Sara","O'Connor","02/27/1999","Male","specific"}};
	    String [][] table18  = { {"<firstName>","<lastName>","<dateOfBirth>","<phoneNumber>"}};
	    String [][] table19  = { {"John-carter","","02/27/2010",""},{"D'Juan","Smith","02/27/1999",""},{""," O-Connor","",""},{""," O'Connor","",""}};
	    String [][] table20  = { {"05/02/2100 "}};
	    String [][] table21  = { {"Robert","Anderson","02/27/1999","Male","3128782504","reference"}};
	    String [][] table22  = { {"< phoneNumber>"}};
	    String [][] table23  = { {"31287825"},{"312878"},{"3128782504000"}};
	    List<String[][]> tables = new ArrayList<>(); 
	    tables.add(table1);
	    tables.add(table2);
	    tables.add(table3);
	    tables.add(table4);
	    tables.add(table5);
	    tables.add(table6);
	    tables.add(table7);
	    tables.add(table8);
	    tables.add(table9);
	    tables.add(table10);
	    tables.add(table11);
	    tables.add(table12);
	    tables.add(table13);
	    tables.add(table14);
	    tables.add(table15);
	    tables.add(table16);
	    tables.add(table17);
	    tables.add(table18);
	    tables.add(table19);
	    tables.add(table20);
	    tables.add(table21);
	    tables.add(table22);
	    tables.add(table23);
		return tables;
	}
	
	private void evaluateTables(String str, TokenMatcher matcher, Parser<GherkinDocument> parser,
			List<String[][]> tables) {
		int i = 0;
	    GherkinDocument gherkinDocument = parser.parse(str, matcher);
	    
	    for (ScenarioDefinition scenarioDef : gherkinDocument.getFeature().getChildren()){
	    	for (Step step : scenarioDef.getSteps()){
	    		if (step.getArgument() instanceof DataTable){
	    		  DataTable dataTable = (DataTable)step.getArgument();
	    		  int row =0;
	    		  int col =0;
				  dataTableCheck(tables, i, dataTable, row, col);
				  i++;
	    		}
	    	}
		  i = examplesCheck(tables, i, scenarioDef);
	    }
	}


	private void dataTableCheck(List<String[][]> tables, int i, DataTable dataTable, int row, int col) {
		for (TableRow tableRow : dataTable.getRows()){
			 if (dataTable.getRows().indexOf(tableRow)!=0){
				 for (TableCell cell : tableRow.getCells()){
					 Assert.assertEquals(cell.getValue(), tables.get(i)[row][col].trim());
					 col++;
				 }
				 row++;
				 col=0;
			 }
		  }
	}


	private int examplesCheck(List<String[][]> tables, int i, ScenarioDefinition scenarioDef) {
		if ( scenarioDef instanceof ScenarioOutline){
			  ScenarioOutline scenarioOutline =(ScenarioOutline) scenarioDef;
			  for (Examples example : scenarioOutline.getExamples()){
	    		  int row =0;
	    		  int col =0;
				  for (TableRow tableRow : example.getTableBody()){
					 for (TableCell cell : tableRow.getCells()){
						 Assert.assertEquals(cell.getValue(), tables.get(i)[row][col].trim());
						 col++;
					 }
					 row++;
					 col=0;					 
				  }
				  i++;
	    	  }
		  }
		return i;
	}
	
	public MavenProject createMavenProject(File basedir)  {
	    Properties properties = new Properties(); 
	    properties.put("version", "1.0"); 		
	    File pom = new File(basedir, "pom.xml"); 
	    MavenExecutionRequest request = new DefaultMavenExecutionRequest(); 
	    request.setUserProperties(properties); 
	    request.setBaseDirectory(basedir); 
	    ProjectBuildingRequest configuration = request.getProjectBuildingRequest(); 
	    configuration.setRepositorySession(new DefaultRepositorySystemSession()); 
	    MavenProject mavenProject=null;
		try {
			mavenProject = this.rule.lookup(ProjectBuilder.class).build(pom, configuration).getProject();
		} catch (ProjectBuildingException|ComponentLookupException e) {
			logger.error("Error creating maven project " , e);
		} 
	    Assert.assertNotNull(mavenProject); 
	    return mavenProject;		
	}
	
	@After
	public final void deleteGeneratedFile() throws NoSuchFileException {
		logger.info("***** DELETE GENERATED FILE ****");	
		File basedir = new File(baseDirPath);
		File featureFile = new File(basedir, "createCustomer.feature"); 
		if (featureFile.exists()){
			featureFile.delete();
		}else {
			logger.info("File does not exist");
		}
	}

				
	
	
}
