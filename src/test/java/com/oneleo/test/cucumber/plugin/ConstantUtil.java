package com.oneleo.test.cucumber.plugin;

public final class ConstantUtil {
	
	private ConstantUtil(){
		
	}

	public static final String USER_DIR_PROPERTY = "user.dir";
	public static final String POM_REL_PATH = "/pom.xml";
	public static final String PROJECT_TEST_PATH = "/src/test/resources/cucumber-support-launcher";
	public static final String VERSION = "version";
	public static final String COMPLEX_EXTERNAL_XLSX_AUTOMATIC_SUPPORT= "/complex-external-xlsx-automatic-support" ;
	public static final String EXTERNAL_XLSX_AUTOMATIC_SUPPORT= "/external-xlsx-automatic-support" ;
	public static final String EXTERNAL_CSV_SUPPORT= "/external-csv-support" ;
	public static final String EXTERNAL_XLSX_MULTIPLE_SHEETS_AUTOMATIC_SUPPORT= "/external-xlsx-multiple-sheets-automatic-support" ;
	
}
