package com.oneleo.test.cucumber.plugin;

import java.io.File;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.grega.test.cucumber.plugin.FileAppender;
import com.grega.test.cucumber.plugin.XLSFileAppender;
import com.grega.test.cucumber.plugin.error.FeatureFileGenerationException;

public class XLSFileAppenderTest {
  
  
  @Test
  public void testFileRead() throws FeatureFileGenerationException {
    FileAppender appender = new XLSFileAppender();
    StringBuilder sb = new StringBuilder();
    sb.append("|abcd|2|3|4|5|").append("\n\r");
    sb.append("|this|is|a|great|test|").append("\n\r");
    
    
    URL url = this.getClass().getResource("/data.xlsx");
    StringBuilder result = appender.appendData(new StringBuilder(), new File(url.getPath()));
    String actual = StringUtils.trimToEmpty(result.toString());
    String expected = StringUtils.trimToEmpty(sb.toString());
    Assert.assertEquals(expected, actual);
    
  }

}
