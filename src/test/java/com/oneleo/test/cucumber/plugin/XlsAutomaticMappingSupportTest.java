package com.oneleo.test.cucumber.plugin;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.maven.execution.DefaultMavenExecutionRequest;
import org.apache.maven.execution.MavenExecutionRequest;
import org.apache.maven.plugin.testing.MojoRule;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuilder;
import org.apache.maven.project.ProjectBuildingException;
import org.apache.maven.project.ProjectBuildingRequest;
import org.codehaus.plexus.component.repository.exception.ComponentLookupException;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.grega.test.cucumber.plugin.ProcessorMojo;

import gherkin.AstBuilder;
import gherkin.Parser;
import gherkin.TokenMatcher;
import gherkin.ast.DataTable;
import gherkin.ast.Examples;
import gherkin.ast.GherkinDocument;
import gherkin.ast.ScenarioDefinition;
import gherkin.ast.ScenarioOutline;
import gherkin.ast.Step;
import gherkin.ast.TableCell;
import gherkin.ast.TableRow;
public class XlsAutomaticMappingSupportTest {
	private Logger logger = LoggerFactory.getLogger(XlsAutomaticMappingSupportTest.class);

	@Rule
	public MojoRule rule = new MojoRule();
	
	private MavenProject project;
	
	private static final String GENERATE_GOAL = "process";
	private String projectDir = System.getProperty(ConstantUtil.USER_DIR_PROPERTY);
	private String baseDirPath; 
	
	
	@Before
	public final void prepareEnvironment(){
		logger.info("***** SETUP Environment *****");	
		this.baseDirPath = projectDir + ConstantUtil.PROJECT_TEST_PATH+ ConstantUtil.EXTERNAL_XLSX_AUTOMATIC_SUPPORT;
		File basedir = new File(baseDirPath);
		this.project = createMavenProject(basedir);
	}
	

	@Test
	public void testSuccessGeneration() throws Exception {
		
		logger.info("***** STARTING TEST WITH CORRECT GENERATION");	
		ProcessorMojo processorMojo = (ProcessorMojo) this.rule.lookupConfiguredMojo(this.project, GENERATE_GOAL);		
		Assert.assertNotNull(processorMojo);
		processorMojo.execute();
		
		File basedir = new File(baseDirPath);
		File featureFile = new File(basedir, "createCustomer.feature"); 
		Assert.assertTrue(featureFile.exists());
		
		logger.info("***** SUCCESS GENERATION");
		
	    String str = FileUtils.readFileToString(featureFile,StandardCharsets.UTF_8);
	    TokenMatcher matcher = new TokenMatcher();
	    Parser<GherkinDocument> parser = new Parser<>(new AstBuilder());
	   
	    List<String[][]> tables = initializeData();
	    
	    
	    evaluateTables(str, matcher, parser, tables);
		
	}


	private List<String[][]> initializeData() {
		String [][] datatable1  = { 
	    		{"104","ENGLISH","standard","44","WBA","ALLIANCE Global"},
	    		{"105","ENGLISH","standard","45","WBA","ALLIANCE Global"},
	    		{"106","ENGLISH","standard","46","WBA","ALLIANCE Global"},
	    		{"107","ENGLISH","standard","47","WBA","ALLIANCE Global"},
	    		{"108","ENGLISH","standard","48","WBA","ALLIANCE Global"}
	                }; 
	    
	    String [][] examples1  = { 
	    		{"109","ENGLISH","standard","49","WBA","ALLIANCE Global"},
	    		{"110","ENGLISH","standard","50","WBA","ALLIANCE Global"},
	    		{"111","ENGLISH","standard","51","WBA","ALLIANCE Global"}
	                }; 	    
	    
	    String [][] examples2  = { 
	    		{"112","ENGLISH","standard","52","WBA","ALLIANCE Global"},
	    		{"113","ENGLISH","standard","53","WBA","ALLIANCE Global"}
	                }; 
	    
	    String [][] examples3  = { 
	    		{"114","ENGLISH","standard","54","WBA","ALLIANCE Global"},
	    		{"115","ENGLISH","standard","55","WBA","ALLIANCE Global"},
	    		{"116","ENGLISH","standard","56","WBA","ALLIANCE Global"},
	    		{"117","ENGLISH","standard","57","WBA","ALLIANCE Global"}
	                }; 	    
	    
	    List<String[][]> tables = new ArrayList<>(); 
	    tables.add(datatable1);
	    tables.add(examples1);
	    tables.add(examples2);
	    tables.add(examples3);
		return tables;
	}


	private void evaluateTables(String str, TokenMatcher matcher, Parser<GherkinDocument> parser,
			List<String[][]> tables) {
		int i = 0;
	    GherkinDocument gherkinDocument = parser.parse(str, matcher);
	    
	    for (ScenarioDefinition scenarioDef : gherkinDocument.getFeature().getChildren()){
	    	for (Step step : scenarioDef.getSteps()){
	    		if (step.getArgument() instanceof DataTable){
	    		  DataTable dataTable = (DataTable)step.getArgument();
	    		  int row =0;
	    		  int col =0;
				  dataTableCheck(tables, i, dataTable, row, col);
				  i++;
	    		}
	    	}
		  i = examplesCheck(tables, i, scenarioDef);
	    }
	}


	private void dataTableCheck(List<String[][]> tables, int i, DataTable dataTable, int row, int col) {
		for (TableRow tableRow : dataTable.getRows()){
			 if (dataTable.getRows().indexOf(tableRow)!=0){
				 for (TableCell cell : tableRow.getCells()){
					 Assert.assertEquals(cell.getValue(), tables.get(i)[row][col].trim());
					 col++;
				 }
				 row++;
				 col=0;
			 }
		  }
	}


	private int examplesCheck(List<String[][]> tables, int i, ScenarioDefinition scenarioDef) {
		if ( scenarioDef instanceof ScenarioOutline){
			  ScenarioOutline scenarioOutline =(ScenarioOutline) scenarioDef;
			  for (Examples example : scenarioOutline.getExamples()){
	    		  int row =0;
	    		  int col =0;
				  for (TableRow tableRow : example.getTableBody()){
					 for (TableCell cell : tableRow.getCells()){
						 Assert.assertEquals(cell.getValue(), tables.get(i)[row][col].trim());
						 col++;
					 }
					 row++;
					 col=0;					 
				  }
				  i++;
	    	  }
		  }
		return i;
	}
	
	
	public MavenProject createMavenProject(File basedir)  {
	    Properties properties = new Properties(); 
	    properties.put("version", "1.0"); 		
	    File pom = new File(basedir, "pom.xml"); 
	    MavenExecutionRequest request = new DefaultMavenExecutionRequest(); 
	    request.setUserProperties(properties); 
	    request.setBaseDirectory(basedir); 
	    ProjectBuildingRequest configuration = request.getProjectBuildingRequest(); 
	    configuration.setRepositorySession(new DefaultRepositorySystemSession()); 
	    MavenProject mavenProject=null;
		try {
			mavenProject = this.rule.lookup(ProjectBuilder.class).build(pom, configuration).getProject();
		} catch (ProjectBuildingException|ComponentLookupException e) {
			logger.error("Error creating maven project " , e);
		} 
	    Assert.assertNotNull(mavenProject); 
	    return mavenProject;		
	}
	
	@After
	public final void deleteGeneratedFile() throws NoSuchFileException {
		logger.info("***** DELETE GENERATED FILE ****");	
		File basedir = new File(baseDirPath);
		File featureFile = new File(basedir, "createCustomer.feature"); 
		if (featureFile.exists()){
			featureFile.delete();
		}else {
			logger.info("File does not exist");
		}
	}
				
	
	
}
