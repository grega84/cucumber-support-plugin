package com.oneleo.test.cucumber.plugin;

import java.io.File;
import java.nio.file.NoSuchFileException;
import java.util.Properties;

import org.apache.maven.execution.DefaultMavenExecutionRequest;
import org.apache.maven.execution.MavenExecutionRequest;
import org.apache.maven.plugin.testing.MojoRule;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuilder;
import org.apache.maven.project.ProjectBuildingException;
import org.apache.maven.project.ProjectBuildingRequest;
import org.codehaus.plexus.component.repository.exception.ComponentLookupException;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.grega.test.cucumber.plugin.ProcessorMojo;
public class CsvMappingSupportTest {
	private Logger logger = LoggerFactory.getLogger(CsvMappingSupportTest.class);

	@Rule
	public MojoRule rule = new MojoRule();
	
	private MavenProject mavenProject;
	
	private static final String GENERATE_GOAL = "process";
	private String projectDir = System.getProperty(ConstantUtil.USER_DIR_PROPERTY);
	private String baseDirPath; 
	
	
	@Before
	public final void prepareEnvironment() {
		

		logger.info("***** SETUP Environment *****");	
		
		this.baseDirPath = projectDir + ConstantUtil.PROJECT_TEST_PATH+ ConstantUtil.EXTERNAL_CSV_SUPPORT;
		File basedir = new File(baseDirPath);
		this.mavenProject = createMavenProject(basedir);
		
	}
	

	@Test
	public void testSuccessGeneration() throws Exception {
		
		logger.info("***** STARTING TEST WITH CORRECT GENERATION");	
		ProcessorMojo processorMojo = (ProcessorMojo) this.rule.lookupConfiguredMojo(this.mavenProject, GENERATE_GOAL);		
		Assert.assertNotNull(processorMojo);
		processorMojo.execute();
		
		File basedir = new File(baseDirPath);
		File featureFile = new File(basedir, "createCustomer.feature"); 
		Assert.assertTrue(featureFile.exists());
		
		logger.info("***** SUCCESS GENERATION");
		
		
	}



	
	public MavenProject createMavenProject(File basedir)   {
	    Properties properties = new Properties(); 
	    properties.put("version", "1.0"); 		
	    File pom = new File(basedir, "pom.xml"); 
	    MavenExecutionRequest request = new DefaultMavenExecutionRequest(); 
	    request.setUserProperties(properties); 
	    request.setBaseDirectory(basedir); 
	    ProjectBuildingRequest configuration = request.getProjectBuildingRequest(); 
	    configuration.setRepositorySession(new DefaultRepositorySystemSession()); 
	    MavenProject project=null;
		try {
			project = this.rule.lookup(ProjectBuilder.class).build(pom, configuration).getProject();
		} catch (ProjectBuildingException | ComponentLookupException e) {
			logger.error("Error creating maven project " , e);
		}
	    Assert.assertNotNull(project); 
	    return project;		
	}
	
	@After
	public final void deleteGeneratedFile() throws NoSuchFileException {
		logger.info("***** DELETE GENERATED FILE ****");	
		File basedir = new File(baseDirPath);
		File featureFile = new File(basedir, "createCustomer.feature"); 
		if (featureFile.exists()){
			featureFile.delete();
		}else {
			logger.info("File does not exist");
		}
	}
				
	
	
}
